import React, {useEffect, useState} from 'react';
import axiosPage from "../../axiosPage";
import InfoPage from "./InfoPage/InfoPage";
import {Container} from 'reactstrap';
import WithErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";

const Info = props => {
    const [info, setInfo] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosPage.get('pages/' + props.match.params.name + '.json');
            setInfo(response.data);
        };
        fetchData().catch(console.error);
    }, [props.match.params.name]);

    return (
            <Container>
                <InfoPage
                    title={info.title}
                    content={info.content}
                />
            </Container>
    );
};

export default WithErrorHandler(Info, axiosPage);