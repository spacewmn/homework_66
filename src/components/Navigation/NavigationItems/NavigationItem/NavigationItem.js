import React from "react";
import {NavItem, NavLink} from "reactstrap";


const NavigationItem = props => (
    <NavItem>
        <NavLink href={props.to}>{props.children}</NavLink>
    </NavItem>
);

export default NavigationItem;