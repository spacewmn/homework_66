import React, {useEffect, useState} from 'react';
import axiosPage from "../../axiosPage";
import AdminForm from "./AdminForm/AdminForm";

const Admin = props => {
    const [infoPage, setInfoPage] = useState(null);
    const [pageName, setPageName] = useState('about');


    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosPage.get('pages/' + pageName + '.json');
            setInfoPage(response.data);
        };
        fetchData().catch(console.error);
    }, [pageName]);

    console.log(infoPage);

    const onSubmit = async (e) => {
        e.preventDefault();
        await axiosPage.put('pages/' + pageName + '.json', infoPage);
        props.history.push('/pages/' + pageName);
    };

    const changeHandler = (e) => {
        let info = {...infoPage};
        if (e.target.name === 'title') {
            info.title = e.target.value;
        }
        if(e.target.name === 'content') {
            info.content = e.target.value;
        }
        setInfoPage(info)
    }

    return (
        infoPage && <AdminForm
            changeHandler={changeHandler}
            pageName={pageName}
            setPageName={setPageName}
            editInfo={infoPage}
            onSubmit={onSubmit}
        />
    );
};

export default Admin;