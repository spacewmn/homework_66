import React from 'react';
import {Container, Button, Form, FormGroup, Label, Input } from 'reactstrap';


const AdminForm = (props) => {

    return (
        <Container>
            <Form onSubmit={props.onSubmit}>
                <FormGroup>
                    <Label for="exampleSelect">Select</Label>
                    <Input type="select" name="select" id="exampleSelect" value={props.pageName} onChange={(e) => props.setPageName(e.target.value)}>
                        <option value='about'>About</option>
                        <option value='production'>Production</option>
                        <option value='home'>Home</option>
                        <option value="contacts">Contacts</option>
                        <option value="support">Support</option>
                    </Input>
                </FormGroup>

                <FormGroup>
                    <Label for='name'>Title</Label>
                    <Input type='text' id='name' name='title' value={props.editInfo.title} onChange={(e) => props.changeHandler(e)} required/>
                </FormGroup>

                <FormGroup>
                    <Label for="content">Content</Label>
                    <Input type="textarea" name="content" id="content" value={props.editInfo.content} onChange={(e) => props.changeHandler(e)} required/>
                </FormGroup>
                <Button type="submit">Submit</Button>
            </Form>
        </Container>
    );
};

export default AdminForm;