import React, {Fragment} from 'react';
import NavigationItems from "../Navigation/NavigationItems/NavigationItems";

const Layout = props => {
    return (
        <Fragment>
            <header>
                <NavigationItems />
            </header>
            <main className="Layout-Content">
                {props.children}
            </main>
        </Fragment>
    );
};

export default Layout;