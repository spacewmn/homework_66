import React from 'react';
import './App.css';
import Layout from "./components/Layout/Layout";
import {Route, Switch} from 'react-router-dom';
import Admin from "./components/Admin/Admin";
import Info from "./components/Info/Info";

const App = () => {
  return (
      <Layout>
          <Switch>
              <Route path="/admin" component={Admin}/>
              <Route path="/pages/:name" exact component={Info}/>
          </Switch>
      </Layout>
   );
};

export default App;

